<?php
    
use App\Models\{
    Plan,
    PlanDetail
};
use Illuminate\Database\Seeder;
    
class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id   = mt_rand(1, '123456789');
        $date = date('Y-m-d H:i:s');
        
        /**
         * Plan 01
         */
        Plan::create([
            'id' => $id,
            'name' => 'Plano Free',
            'slug' => 'plano-free',
            'price' => 0.00,
            'description' => 'Acesso grátis por 30 dias.',
            'order' => '01',
            'active' => 1,
            'created_at' => $date
        ]);
        
        /**
         * Plano 02
         */
        Plan::create([
            'id' => $id+1,
            'name' => 'Plano Ouro',
            'slug' => 'plano-ouro',
            'price' => 99.00,
            'description' => 'Acesso a todos os modulos.',
            'order' => '02',
            'active' => 1,
            'created_at' => $date
        ]);
        
        /**
         * Palno 03
         */
        Plan::create([
            'id' => $id+2,
            'name' => 'Plano Prata',
            'slug' => 'plano-prata',
            'price' => 69.00,
            'description' => 'Acesso Restrito.',
            'order' => '03',
            'active' => 1,
            'created_at' => $date
        ]);
        
        /**
         * Plano 04
         */
        Plan::create([
            'id' => $id+3,
            'name' => 'Plano Bronze',
            'slug' => 'plano-bronze',
            'price' => 49.00,
            'description' => 'Acesso Restrito.',
            'order' => '04',
            'active' => 1,
            'created_at' => $date
        ]);
        
        
        /**
         * Recursos plano 1
         */
        PlanDetail::create([
            'plan_id' => $id,
            'resource' => 'Web Desktop',
            'icon' => 'monitor',
            'iconcolor' => 'info',
            'active' => 1,
            'order' => '01',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id,
            'resource' => 'Celular',
            'icon' => 'smartphone',
            'iconcolor' => 'black',
            'active' => 1,
            'order' => '02',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id,
            'resource' => 'Tablet',
            'icon' => 'tablet',
            'iconcolor' => 'warning',
            'active' => 1,
            'order' => '03',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id,
            'resource' => 'Video',
            'icon' => 'video',
            'iconcolor' => 'danger',
            'active' => 1,
            'order' => '04',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id,
            'resource' => 'Fotos',
            'icon' => 'camera',
            'iconcolor' => 'primary',
            'active' => 1,
            'order' => '05',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id,
            'resource' => 'Mensagens',
            'icon' => 'message-circle',
            'iconcolor' => 'success',
            'active' => 1,
            'order' => '06',
            'created_at' => $date
        ]);
        
        /**
         * Recursos plano 2
         */
        PlanDetail::create([
            'plan_id' => $id+1,
            'resource' => 'Web Desktop',
            'icon' => 'monitor',
            'iconcolor' => 'info',
            'active' => 1,
            'order' => '01',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+1,
            'resource' => 'Celular',
            'icon' => 'smartphone',
            'iconcolor' => 'black',
            'active' => 1,
            'order' => '02',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+1,
            'resource' => 'Tablet',
            'icon' => 'tablet',
            'iconcolor' => 'warning',
            'active' => 1,
            'order' => '03',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+1,
            'resource' => 'Video',
            'icon' => 'video',
            'iconcolor' => 'danger',
            'active' => 1,
            'order' => '04',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+1,
            'resource' => 'Fotos',
            'icon' => 'camera',
            'iconcolor' => 'primary',
            'active' => 1,
            'order' => '05',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+1,
            'resource' => 'Mensagens',
            'icon' => 'message-circle',
            'iconcolor' => 'success',
            'active' => 1,
            'order' => '06',
            'created_at' => $date
        ]);
        
        /**
         * Recursos plano 3
         */
        PlanDetail::create([
            'plan_id' => $id+2,
            'resource' => 'Web Desktop',
            'icon' => 'monitor',
            'iconcolor' => 'info',
            'active' => 1,
            'order' => '01',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+2,
            'resource' => 'Celular',
            'icon' => 'smartphone',
            'iconcolor' => 'black',
            'active' => 1,
            'order' => '02',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+2,
            'resource' => 'Tablet',
            'icon' => 'tablet',
            'iconcolor' => 'warning',
            'active' => 1,
            'order' => '03',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+2,
            'resource' => 'Video',
            'icon' => 'video',
            'iconcolor' => 'danger',
            'active' => 1,
            'order' => '04',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+2,
            'resource' => 'Fotos',
            'icon' => 'camera',
            'iconcolor' => 'primary',
            'active' => 1,
            'order' => '05',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+2,
            'resource' => 'Mensagens',
            'icon' => 'message-circle',
            'iconcolor' => 'success',
            'active' => 1,
            'order' => '06',
            'created_at' => $date
        ]);
        
        
        /**
         * Recursos plano 4
         */
        PlanDetail::create([
            'plan_id' => $id+3,
            'resource' => 'Web Desktop',
            'icon' => 'monitor',
            'iconcolor' => 'info',
            'active' => 1,
            'order' => '01',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+3,
            'resource' => 'Celular',
            'icon' => 'smartphone',
            'iconcolor' => 'black',
            'active' => 1,
            'order' => '02',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+3,
            'resource' => 'Tablet',
            'icon' => 'tablet',
            'iconcolor' => 'warning',
            'active' => 1,
            'order' => '03',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+3,
            'resource' => 'Video',
            'icon' => 'video',
            'iconcolor' => 'danger',
            'active' => 1,
            'order' => '04',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+3,
            'resource' => 'Fotos',
            'icon' => 'camera',
            'iconcolor' => 'primary',
            'active' => 1,
            'order' => '05',
            'created_at' => $date
        ]);
        PlanDetail::create([
            'plan_id' => $id+3,
            'resource' => 'Mensagens',
            'icon' => 'message-circle',
            'iconcolor' => 'success',
            'active' => 1,
            'order' => '06',
            'created_at' => $date
        ]);
    }
}
