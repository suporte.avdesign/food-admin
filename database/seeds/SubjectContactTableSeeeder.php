<?php

use App\Models\SubjectContact;
use Illuminate\Database\Seeder;

class SubjectContactTableSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubjectContact::create([
            'title' => 'Asinatura de Planos',
            'department' => 'vendas',
            'name' => 'Anselmo Velame',
            'phone' => '(11) 93209-2772',
            'email' => 'design@anselmovelame.com.br'
        ]);
        SubjectContact::create([
            'title' => 'Outros',
            'department' => 'outros',
            'name' => 'Anselmo Velame',
            'phone' => '(11) 93209-2772',
            'email' => 'suporte.avdesign@gmail.com'
        ]);
    }
}
