<?php

use App\Models\ACL\Profile;
use Illuminate\Database\Seeder;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id   = mt_rand(1, '123456789');
        $date = date('Y-m-d H:i:s');
    
        Profile::create([
            'id' => $id,
            'name' => 'Master',
            'slug' => 'master',
            'icon' => 'feather icon-user-plus',
            'description' => 'Super Admin.',
            'created_at' => $date
        ]);
        Profile::create([
            'id' => $id+1,
            'name' => 'Admin',
            'slug' => 'admin',
            'icon' => 'feather icon-users',
            'description' => 'Administrador do sistema.',
            'created_at' => $date
        ]);
        Profile::create([
            'id' => $id+3,
            'name' => 'Editor',
            'slug' => 'editor',
            'icon' => 'feather icon-user-check',
            'description' => 'Editor do sistema.',
            'created_at' => $date
        ]);
        Profile::create([
            'id' => $id+4,
            'name' => 'Cliente',
            'slug' => 'cliente',
            'icon' => 'feather icon-user',
            'description' => 'Cliente do sistema.',
            'created_at' => $date
        ]);
        
    }
}
