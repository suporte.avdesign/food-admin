<?php

use App\Models\Company;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id   = mt_rand(1, '123456789');
        $date = date('Y-m-d H:i:s');
        
        $company = Company::first();
    
        $company->users()->create([
            'id' => $id,
            'name' => 'Anselmo Velame',
            'email' => 'design@anselmovelame.com.br',
            'password' => bcrypt('avdesign'),
            'created_at' => $date
        ]);
    }
}
