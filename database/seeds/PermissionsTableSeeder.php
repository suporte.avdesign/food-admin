<?php

use App\Models\ACL\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id   = mt_rand(1, '123456789');
        $date = date('Y-m-d H:i:s');
    
        Permission::create([
            'id' => $id,
            'name' => 'Permissões',
            'slug' => 'permissoes',
            'description' => 'Permite alterar as permissões do sistema.',
            'created_at' => $date
        ]);
        Permission::create([
            'id' => $id+1,
            'name' => 'Perfis',
            'slug' => 'perfis',
            'description' => 'Permite editar os perfis do sistema.',
            'created_at' => $date
        ]);
        
    }
}
