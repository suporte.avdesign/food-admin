<?php

use App\Models\Plan;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id   = mt_rand(1, '123456789');
        $date = date('Y-m-d H:i:s');
    
        $plan = Plan::first();
        $plan->companies()->create([
            'id' => $id,
            'uuid' => Str::uuid(),
            'name' => 'Churrascaria do Gaucho',
            'email' => 'company@teste.com',
            'phone' => '11932092772',
            'document' => '31.958.078/0001-47',
            'slug' => 'churrascaria-do-gaucho',
            'created_at' => $date
        ]);
    }
}
