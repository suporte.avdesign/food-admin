<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('plan_id'); # Identificação do Plano
            $table->uuid('uuid'); # Identificação da Empresa.
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('document')->unique();
            $table->string('slug')->unique();
            $table->string('logo')->nullable();
            $table->enum('active', ['Y', 'N'])->default('Y'); # Status se 'N' ele perde o acesso ao sistema.
            //Subscription
            $table->date('subscription')->nullable(); # Data que se inscreveu.
            $table->date('expires_at')->nullable(); # Data que expira o acesso.
            $table->string('subscription_id', 255)->nullable(); # Identificação do Gateway de pagamento.
            $table->boolean('subscription_active')->default(false); # Assinatura ativa (porque
            $table->boolean('subscription_suspended')->default(false); # Assinatura cancelada
            $table->timestamps();
            
            //FOREIGN KEY
            $table->foreign('plan_id')->references('id')->on('plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
