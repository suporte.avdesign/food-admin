<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('subject_contact_id');
            $table->string('company',100);
            $table->string('name',100);
            $table->string('phone',20);
            $table->string('email',100);
            $table->longText('message');
            $table->timestamps();
    
            $table->foreign('subject_contact_id')
                ->references('id')
                ->on('subject_contacts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
