<?php
/**
 * Auth Routes
 */
Auth::routes();

/**
 * Routes Users.
 */
Route::resource('usuarios', 'UserController');

/**
 * Routes ACL.
 */
Route::resource('permissoes', 'ACL\PermissionController');
Route::resource('perfis', 'ACL\ProfileController');


Route::get('plans/{slug}/profiles', 'ACL\PlanProfileController@profiles')->name('plans.profiles');
Route::get('plans/{slug}/profiles/create', 'ACL\PlanProfileController@profilesAvailable');
Route::post('plans/{slug}/profiles/store', 'ACL\PlanProfileController@attachPermissionsProfile');
Route::get('plans/{slug}/profile/{id}/detach', 'ACL\PlanProfileController@detachPermissionsProfile');
/**
 * Adicionar e remover permissões aos perfis
 */
Route::get('profile/{slug}/permissions', 'ACL\ProfilePermissionController@permissions')->name('profiles.permissions');
Route::get('profile/{slug}/permissions/create', 'ACL\ProfilePermissionController@permissionsAvailable');
Route::post('profile/{slug}/permissions/store', 'ACL\ProfilePermissionController@attachPermissionsProfile');
Route::get('profile/{slug}/permissions/{id}/detach', 'ACL\ProfilePermissionController@detachPermissionsProfile');
//Visualizar as permissões dos perfis.
Route::get('permissions/{slug}/profiles', 'ACL\ProfilePermissionController@profiles')->name('permissions.profiles');

/**
 * Routes Planos, Recursos e Perfis dos Planos.
 */
Route::resource('planos', 'PlanController', ['except' => ['edit']]);
Route::resource('planos/{slug}/details', 'PlanDetailsController', ['except' => ['create', 'edit']]);
// Adicionar e remover perfis aos planos.
Route::resource('planos/{slug}/profiles', 'ACL\PlanProfileController', ['except' => ['create', 'edit']]);

// Route url
Route::get('/', 'DashboardController@dashboardAnalytics');

// Route Dashboards
Route::get('/dashboard-analytics', 'DashboardController@dashboardAnalytics');

// Route Components
Route::get('/sk-layout-2-columns', 'StaterkitController@columns_2');
Route::get('/sk-layout-fixed-navbar', 'StaterkitController@fixed_navbar');
Route::get('/sk-layout-floating-navbar', 'StaterkitController@floating_navbar');
Route::get('/sk-layout-fixed', 'StaterkitController@fixed_layout');

