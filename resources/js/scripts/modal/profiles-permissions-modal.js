/*=========================================================================================
    File Name: profiles-profiles-permissions-modal.js
    Description: Open Profiles-Permissions
    ----------------------------------------------------------------------------------------
    Item Name: Painel  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: AV DESIGN
    Author URL: http://www.avdesign.com.br
==========================================================================================*/
(function(window, document, $) {
    'use strict';

    $("#modalPermissions").on("show.bs.modal", function(e) {
        let btn = $(e.relatedTarget);
        $(this).find(".modal-body").load(btn.attr("href"));
    });

})(window, document, jQuery);
