/*=========================================================================================
    File Name: permissions.js
    Description: Add and edit system module permissions
    ----------------------------------------------------------------------------------------
    Item Name: Permission  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: AV DESIGN
    Author URL: http://www.avdesign.com.br
==========================================================================================*/
(function(window, document, $) {
    'use strict';

    let  configModule = _configPermissions;

    // init list view datatable
    let dataListView = $(".data-list-view").DataTable({
        responsive: false,
        columnDefs: [
            {
                orderable: true,
                targets: 0,
            }
        ],
        dom:
            '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
        oLanguage: {
            sLengthMenu: "_MENU_",
            sSearch: ""
        },
        aLengthMenu: configModule.dataTable.aLengthMenu,
        select: { style: "multi"},
        order: configModule.dataTable.order,
        bInfo: false,
        pageLength: configModule.dataTable.pageLength,
        buttons: [
            {
                text: "<i class='feather icon-plus'></i> "+configModule.textAdd,
                action: function() {
                    $(this).removeClass("btn-secondary")
                    $("#data-name, #data-description").val("")
                },
                className: "btn-primary action-add"
            }
        ],
        initComplete: function(settings, json) {
            $(".dt-buttons .btn").removeClass("btn-secondary")
        }
    });

    /**
     * Close modal
     */
    $('.action-close').on("click", function(e){
        e.stopPropagation();
        $("#data-name, #data-description").val("");
        $('#modalPermissions').modal('hide');
    });

    /**
     * Form create permission
     */
    $('.action-add').click(function(){
        $.ajax({
            type: 'GET',
            dataType: "html",
            url: configModule.url+'/create',
            beforeSend: function() {
                $('.action-add').prop("disabled", true);
            },
            success: function(data){
                $('#type-hidden').html('');
                $('.modal-title').html(configModule.textAdd);
                $('.modal-body').html(data);
                $('#modalPermissions').modal('show');
                $('.action-add').prop("disabled", false);
                $('#form-permissions').attr('action', configModule.url);
            },
            error: function (xhr) {
                $('.action-add').prop("disabled", false);
                let msg = errorStatus(xhr, configModule);
                $('#return-messages').html(msg);
            }
        });
    });

    /**
     * Edit Permission
     */
    $('.action-edit').click(function(){
        let Id = $(this).data('id'),
            slug = $(this).data('slug'),
            url = configModule.url +'/'+slug;
        $.ajax({
            type: 'GET',
            dataType: "html",
            url: url,
            success: function(data){
                $('.modal-title').html(configModule.textUpdate);
                $('.modal-body').html(data);
                $('#modalPermissions').modal('show');
                $('#form-permissions').attr('action', url);
                $('#type-hidden').prepend(
                    '<input type="hidden" name="id" value="'+Id+'">'+
                    '<input type="hidden" name="_method" value="put">');
            },
            error: function (xhr) {
                let msg = errorStatus(xhr, configModule);
                $('#return-messages').html(msg);
            }
        });
    });

    /**
     * Delete Permission
     */
    $('.action-delete').click(function(){
        let slug = $(this).data('slug');
        $.ajax({
            type: 'DELETE',
            dataType: "json",
            url: configModule.url +'/'+slug,
            data: {_token: configModule.token},
            success: function(data){
                if (data.success == true) {
                    $('#'+slug).remove();
                }
            },
            error: function (xhr) {
                errorStatus(xhr, configModule);
            }
        });
    });

    /**
     * Form create permission
     */
    // On Subimit
    $('.action-submit').on("click", function(){
        let form = $('#form-permissions'),
            action = form.attr('action');
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: action,
            data: form.serialize(),
            beforeSend: function() {
                $('.action-submit').prop("disabled", true);
            },
            success: function(data){
                if (data.success == true) {
                    let msg = displayAlert('success', data.message)
                    $('#return-messages').html(msg);
                    setTimeout(function() {
                        $('#modalPermissions').modal('hide');
                        window.location.href = configModule.url;
                    }, 3000);

                } else {
                    $('#return-messages').html(displayAlert('danger', data.message));
                }
            },
            error: function(xhr) {
                $('.action-submit').prop("disabled", false);
                let msg = errorStatus(xhr, configModule);
                $('#return-messages').html(msg);
                setTimeout(function() {
                    $('#return-messages').html('');
                }, 3000);
            }
        });
    });

    /**
     * Close sidebar.
     */
    $(".hide-data-sidebar, .cancel-data-btn, .overlay-bg").on("click", function() {
        $(".add-new-data").removeClass("show")
        $(".overlay-bg").removeClass("show")
        $(".data-permission-profiles").html("")
    })


    /**
     * Open specific profile sidebar.
     */
    $('.action-profiles-permission').on("click",function(e){
        e.stopPropagation();
        let slug = $(this).data('slug');
        $.get( `permissions/${slug}/profiles`, function( data ) {
            $('.add-new-data').addClass("show")
            $('.overlay-bg').addClass("show")
            $('.title-header-sidbar').text(configModule.textPermissions+' '+slug)
            $('.data-permission-profiles').html(data)
        });
    });

    // mac chrome checkbox fix
    if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
    }

})(window, document, jQuery);