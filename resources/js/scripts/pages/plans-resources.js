/*=========================================================================================
    File Name: plans-resources.js
    Description: Add and edit plans and plan features
    ----------------------------------------------------------------------------------------
    Item Name: Painel  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: AV DESIGN
    Author URL: http://www.avdesign.com.br
==========================================================================================*/
const  configModule = _configPlans;

(function(window, document, $) {
    'use strict';
    /**
     * Adicionar Plano
     */
    $('.btnAddPlan').click(function(){
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: configModule.url_create,
            success: function(data){
                if (data.success == true) {
                    $('#modalPlans').modal('show');
                    $('.modal-content').html(data.modal);
                }
            },
            error: function (xhr) {
                let msg = errorStatus(xhr);
            }
        });
    });

    /**
     * Editar Plano
     */
    $('.btnEditPlan').click(function(){
        let slug = $(this).data('slug');
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: configModule.url +'/'+slug,
            success: function(data){
                if (data.success == true) {
                    $('#modalPlans').modal('show');
                    $('.modal-content').html(data.modal);
                }
            },
            error: function (xhr) {
                let msg = errorStatus(xhr);
            }
        });
    });

    /**
     * Remover Plano
     */
    $('.btnRemovePlan').click(function(){

        let slug = $(this).data('slug');
        $.ajax({
            type: 'DELETE',
            dataType: "json",
            url: configModule.url +'/'+slug,
            data: {_token: configModule.token},
            success: function(data){
                if (data.success == true) {
                    $('#'+slug).remove();
                }
            },
            error: function (xhr) {
                let msg = errorStatus(xhr);
            }
        });
    });

    /**
     * Adicionar editar e remover recursos dos planos
     */

    // Scrollbar
    if ($(".data-items").length > 0) {
        new PerfectScrollbar(".data-items", { wheelPropagation: false })
    }

    // Close sidebar
    $(".hide-data-sidebar, .cancel-data-btn, .overlay-bg").on("click", function() {
        $(".add-new-data").removeClass("show")
        $(".overlay-bg").removeClass("show")
        $("#data-name").val("")
        $("#data-editor .ql-editor").html("")
        $('#type-hidden').html('');
    })

    // On Add
    $('.action-add').on("click",function(e){
        e.stopPropagation();
        let plan = $(this).data('plan'),
            Id = $(this).data('id');
        $('.add-new-data').addClass("show");
        $('.overlay-bg').addClass("show");
        $('#type-hidden').html('<input type="hidden" name="plan_id" value="'+Id+'">');
        $('#form-plan-resources').attr('action', _configPlans.url+'/'+plan+'/details');

    });

    // On Edit
    $('.action-edit').on("click",function(e){
        e.stopPropagation();
        let plan = $(this).data('plan'),
            Id = $(this).data('id');
        $.get( _configPlans.url+'/'+plan+'/details/'+Id, function( data ) {
            $('#form-plan-resources').attr('action', _configPlans.url+'/'+plan+'/details/'+Id)
            $('.add-new-data').addClass("show");
            $('.overlay-bg').addClass("show");
            $('#data-resource').val(data.resource);
            $('#data-editor .ql-editor').html(data.resource);
            $('#data-icon').val(data.icon);
            $('#data-order').val(data.order);
            $('input[name="iconcolor"]').prop('checked' , false);
            $('#data-'+data.iconcolor).prop('checked' , true);
            $('input[name="active"]').prop('checked' , false);
            $('#data-active-'+data.active).prop('checked' , true);
            $('#type-hidden').prepend(
                '<input type="hidden" id="_method" name="_method" value="put">');
        });
    });

    // On Delete
    $('.action-delete').on("click", function(e){
        e.stopPropagation();
        let plan = $(this).data('plan'),
            Id = $(this).data('id');
        $.ajax({
            type: 'DELETE',
            dataType: "json",
            url: _configPlans.url+'/'+plan+'/details/'+Id,
            data: {_token: _configPlans.token},
            beforeSend: function() {
                $('.action-delete').prop("disabled", true);
            },
            success: function(data){
                if (data.success == true) {
                    $('#resource-'+Id).remove();
                } else {
                    displayAlert('danger', data.message);
                }
            },
            error: function(xhr) {
                $('.action-delete').prop("disabled", false);
                let msg = errorStatus(xhr);
                $('#return-messages').html(msg);
                setTimeout(function() {
                    $('#return-messages').html('');
                }, 3000);
            }
        });
    });

    // On Subimit
    $('.action-subimit').on("click", function(){
        let resource = $('#data-editor .ql-editor').html();
        $('#data-resource').val(resource);

        let form = $('#form-plan-resources'),
            url = form.attr('action');
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: url,
            data: form.serialize(),
            beforeSend: function() {
                $('.action-subimit').prop("disabled", true);
            },
            success: function(data){
                if (data.success == true) {
                    let msg = displayAlert('success', data.message)
                    //$('#btn-reset').trigger('click');
                    $('#return-messages').html(msg);

                    setTimeout(function() {
                        window.location.href = _configPlans.url;
                    }, 3000);

                } else {
                    displayAlert('danger', data.message);
                }
            },
            error: function(xhr) {
                $('.action-subimit').prop("disabled", false);
                let msg = errorStatus(xhr);
                $('#return-messages').html(msg);
                setTimeout(function() {
                    $('#return-messages').html('');
                }, 3000);
            }

        });
    });

    /**
     * Carregar view para adicionar, editar e remover perfil aos planos.
     */
    $('.btnProfiles').on("click",function(e) {

        e.stopPropagation();
        let plan = $(this).data('slug')
        $('.content-header-left').remove()
        $('.content-header-right').remove()
        $('#plans-card').remove()
        $.get( configModule.url+'/'+plan+'/profiles', function( data ) {
            $('#profiles-settings').html( data)
        })
    });


})(window, document, jQuery);

/**
 * Vincular e desvincular perfil a um plano específico.
 * @param index
 * @param slug
 */
function sendPLanProfiles(index, slug) {
    let _url,_method,_data,_label,
        chk = $('#profile-'+index),
        Id = chk.val();
    if (chk.is(':checked')) {
        _method = 'POST';
        _url = `${configModule.url}/${slug}/profiles`;
        _data = {profile: [Id], _token: configModule.token};
        $('.label-profile-'+slug).text('Ativo');
    } else {
        _method = 'DELETE';
        _data = {_token: configModule.token};
        _url = `${configModule.url}/${slug}/profiles/${Id}`;
        $('.label-profile-'+slug).text('Inativo');
    }
    $.ajax({
        type: _method,
        dataType: "json",
        url: _url,
        data: _data,
        beforeSend: function() {
            chk.prop("disabled", true);
        },
        success: function(data){
            if (data.success == true) {
                chk.prop("disabled", false);
                let msg = displayAlert('success', data.message)
                $('#return-messages-profiles').html(msg);

                setTimeout(function() {
                    $('#return-messages-profiles').html('');
                }, 3000);

            } else {
                displayAlert('danger', data.message);
            }
        },
        error: function(xhr) {
            chk.prop("disabled", false);
            let msg = errorStatus(xhr);
            $('#return-messages-profiles').html(msg);
            setTimeout(function() {
                $('#return-messages-profiles').html('');
            }, 3000);
        }
    });
}

function addPLanProfiles(url) {
    $.ajax({
        type: 'GET',
        dataType: "html",
        url: url+'/show',
        success: function(data){
            $('.modal-body').html(data);
            $('#modalPlanProfiles').modal('show');
            $('#form-plan-profiles').attr('action', url);
        },
        error: function (xhr) {
            let msg = errorStatus(xhr, configModule);
            $('#return-messages').html(msg);
        }
    });
}

function createProfiles() {
    let form = $('#form-plan-profiles'),
        url = form.attr('action');
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: url,
        data: form.serialize(),
        beforeSend: function() {
            $('.btn-profile-submit').prop("disabled", true);
        },
        success: function(data){
            if (data.success == true) {
                let msg = displayAlert('success', data.message)
                $('#return-messages-profiles').html(msg);

                setTimeout(function() {
                    window.location.href = configModule.url;
                }, 3000);

            } else {
                displayAlert('danger', data.message);
            }
        },
        error: function(xhr) {
            $('.btn-profile-submit').prop("disabled", false);
            let msg = errorStatus(xhr);
            $('#return-messages-profiles').html(msg);
            setTimeout(function() {
                $('#return-messages-profiles').html('');
            }, 3000);
        }
    });
}

