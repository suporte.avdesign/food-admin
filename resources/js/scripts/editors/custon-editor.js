/*=========================================================================================
	File Name: editor-quill.js
	Description: Quill is a modern rich text editor built for compatibility and extensibility.
	----------------------------------------------------------------------------------------
	Item Name: Painel  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: AV DESIGN
    Author URL: http://www.avdesign.com.br
==========================================================================================*/
(function (window, document, $) {
  'use strict';

  var Font = Quill.import('formats/font');
  Font.whitelist = ['sofia', 'slabo', 'roboto', 'inconsolata', 'ubuntu'];
  Quill.register(Font, true);

  // My Editor
  var customEditor = new Quill('#custon-editor .editor', {
    bounds: '#custon-editor .editor',
    modules: {
      'syntax': true,
      'toolbar': [
        ['bold', 'italic', 'underline', 'strike', {
          'color': []
        }, {
          'background': []
        }],
        [{
          'list': 'ordered'
        }, {
          'list': 'bullet'
        },{
          'align': []
        }],
        ['link']
      ],
    },
    theme: 'snow'
  });

  var editors = [customEditor];

})(window, document, jQuery);
