/*=========================================================================================
    File Name: profiles-permissions.js
    Description: List View
    ----------------------------------------------------------------------------------------
    Item Name: Painel  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: AV DESIGN
    Author URL: http://www.avdesign.com.br
==========================================================================================*/

$(document).ready(function() {
    "use strict"

    // Iniciar lista datatable
    let dataListView = $(".data-list-view").DataTable({
        responsive: false,
        columnDefs: [
            {
                orderable: false,
                targets: 0,
                //checkboxes: { selectRow: true }
            }
        ],
        dom:
            '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
        oLanguage: {
            sLengthMenu: "_MENU_",
            sSearch: ""
        },
        aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
        select: {
            style: "multi"
        },
        order: [[1, "desc"]],
        bInfo: false,
        pageLength: 4,
        buttons: [
            {
                text: "<i class='feather icon-plus'></i> "+ _configProfiles.textAdd,
                action: function() {
                    $(this).removeClass("btn-secondary")
                    $(".add-new-data").addClass("show")
                    $(".overlay-bg").addClass("show")
                    $('#form-profile').attr('action', _configProfiles.url)
                    $("#data-name").val("")
                    $("#data-editor .ql-editor").html("")
                    $('#type-hidden').html('');
                },
                className: "btn-outline-primary"
            }
        ],
        initComplete: function(settings, json) {
            $(".dt-buttons .btn").removeClass("btn-secondary")
        }
    });
    /*
    dataListView.on('draw.dt', function(){
        setTimeout(function(){
            if (navigator.userAgent.indexOf("Mac OS X") != -1) {
                $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
            }
        }, 50);
    });
     */

    // Para anexar o menu suspenso de ações antes de adicionar novo botão
    let actionDropdown = $(".actions-dropodown")
    actionDropdown.insertBefore($(".top .actions .dt-buttons"))


    // Scrollbar
    if ($(".data-items").length > 0) {
        new PerfectScrollbar(".data-items", { wheelPropagation: false })
    }

    // Close sidebar
    $(".hide-data-sidebar, .cancel-data-btn, .overlay-bg").on("click", function() {
        $(".add-new-data").removeClass("show")
        $(".overlay-bg").removeClass("show")
        $("#data-name").val("")
        $("#data-editor .ql-editor").html("")
        $('#type-hidden').html('');
    })


    // On Edit
    $('.action-edit').on("click",function(e){
        e.stopPropagation();
        let Id = $(this).data('id');
        $.get( _configProfiles.url+'/'+Id, function( data ) {
            $('#form-profile').attr('action', _configProfiles.url+'/'+Id)
            $('#data-name').val(data.name);
            $('#data-description').html(data.description);
            $('.add-new-data').addClass("show");
            $('.overlay-bg').addClass("show");
            $('#type-hidden').prepend(
                '<input type="hidden" id="_method" name="_method" value="put">' +
                '<input type="hidden" id="_id" name="id" value="'+Id+'">');
        });
    });

    // On Delete
    $('.action-delete').on("click", function(e){
        e.stopPropagation();

        let td = $(this).closest('td').parent('tr'),
            Id = $(this).data('id');
        $.ajax({
            type: 'DELETE',
            dataType: "json",
            url: _configProfiles.url+'/'+Id,
            data: {_token: _configProfiles.token},
            beforeSend: function() {
                $('.action-delete').prop("disabled", true);
            },
            success: function(data){
                if (data.success == true) {
                    td.fadeOut();
                } else {
                    return false;
                }
            },
            error: function(xhr) {
                $('.action-delete').prop("disabled", false);
                let msg = errorStatus(xhr);
                $('#return-messages').html(msg);
                setTimeout(function() {
                    $('#return-messages').html('');
                }, 3000);
            }
        });
    });

    // On Subimit
    $('.action-subimit').on("click", function(){
        let description = $('#data-editor .ql-editor').html();
        $('#data-description').val(description);

        let form = $('#form-profile'),
            url = form.attr('action');
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: url,
            data: form.serialize(),
            beforeSend: function() {
                $('.action-subimit').prop("disabled", true);
            },
            success: function(data){
                if (data.success == true) {
                    let msg = displayAlert('success', data.message)
                    //$('#btn-reset').trigger('click');
                    $('#return-messages').html(msg);

                    setTimeout(function() {
                        $('#modalPlans').modal('hide');
                        window.location.href = _configProfiles.url;
                    }, 3000);

                } else {

                    return false;
                }
            },
            error: function(xhr) {
                $('.action-subimit').prop("disabled", false);
                let msg = errorStatus(xhr);
                $('#return-messages').html(msg);
                setTimeout(function() {
                    $('#return-messages').html('');
                }, 3000);
            }

        });
    });


    // mac chrome checkbox fix
    if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
    }
});
function errorStatus(xhr) {
    switch (xhr.status) {
        case 422:
            let obj = $.parseJSON(xhr.responseText), message = '';
            $.each( obj, function( key, value ) {
                $("#"+key).addClass('required');
                if (key == 'message') {
                    message += '<h4 class="alert-heading"><strong>Erro: </strong>' + _configProfiles.error + '</h4>';
                } else if (key == 'errors') {
                    $.each(obj[key], function(i, error) {
                        message += '<p>'+error+'</p>';
                    });
                } else {
                    message += '<p>'+value+'</p>';
                }
            });
            return displayAlert('danger', message);
            break;
        default:
            //return displayAlert('danger', `Erro código: ${xhr.status}.`);
            alert(`Erro: ${xhr.status} ${xhr.responseText}.`);
    }
}

function displayAlert(cls, message) {
    return  '<div class="alert alert-'+cls+' mt-1" role="alert">' + message + '</div>';
}
