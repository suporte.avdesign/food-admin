<ul class="nav navbar-nav">
  <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i class="ficon feather icon-star warning"></i></a>
    <div class="bookmark-input search-input">
      <div class="bookmark-input-icon"><i class="feather icon-search primary"></i></div>
      <input class="form-control input" type="text" placeholder="Buscar no Modulo..." tabindex="0" data-search="laravel-starter-list" />
      <ul class="search-list search-list-bookmark"></ul>
    </div>
    <!-- select.bookmark-select-->
    <!--   option 1-Column-->
    <!--   option 2-Column-->
    <!--   option Static Layout-->
  </li>
</ul>
