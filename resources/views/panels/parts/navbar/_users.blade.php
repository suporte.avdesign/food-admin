<li class="dropdown dropdown-user nav-item">
  <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
    <div class="user-nav d-sm-flex d-none">
      <span class="user-name text-bold-600">{{Auth::user()->name}}</span>
      <span class="user-status">Master</span>
    </div>
    <span>
      <img class="round" src="{{asset('images/portrait/small/avatar-s-11.jpg') }}" alt="avatar" height="40" width="40" />
    </span>
  </a>
  <div class="dropdown-menu dropdown-menu-right">
    <a class="dropdown-item" href="javascript:void(0)"><i class="feather icon-user"></i> Editar Perfil</a>
    <a class="dropdown-item" href="javascript:void(0)"><i class="feather icon-mail"></i> Agenda</a>
    <a class="dropdown-item" href="javascript:void(0)"><i class="feather icon-check-square"></i> Tarefas</a>
    <a class="dropdown-item" href="javascript:void(0)"><i class="feather icon-message-square"></i> Bate Papo</a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="javascript:void(0)" onclick="document.getElementById('logout-form').submit();"><i class="feather icon-power"></i> Sair</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST">
      @csrf
    </form>

  </div>
</li>
