<div class="data-items pb-3">
    <div class="data-fields px-2 mt-1">
        <div class="row">
            <div class="col-sm-12 data-field-col">
                <label for="data-resource">Recurso</label>
                <div id="custon-editor">
                    <div id="data-editor" class="editor"></div>
                </div>
                <input type="hidden" id="data-resource" name="resource" value="">
                <div id="return-messages"></div>
            </div>
            <div class="col-sm-12 data-field-col">
                <label for="data-active">Status: </label>
                <ul class="list-unstyled mb-0">
                    <li class="d-inline-block mr-2">
                        <fieldset>
                            <div class="vs-radio-con vs-radio-success">
                                <input type="radio" id="data-active-1" name="active" value="1" checked>
                                <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Ativo</span>
                            </div>
                        </fieldset>
                    </li>
                    <li class="d-inline-block mr-2">
                        <fieldset>
                            <div class="vs-radio-con vs-radio-danger">
                                <input type="radio" id="data-active-0" name="active" value="0">
                                <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Inativo</span>
                            </div>
                        </fieldset>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 data-field-col">
                <label for="data-icon">Icone: </label>
                <input type="text" id="data-icon" name="icon" value="" placeholder="Icone" class="form-control">
            </div>
            <div class="col-sm-12 data-field-col">
                <label for="data-iconcolor">Cor do Icone: </label>
                <ul class="list-unstyled mb-0">
                    <li class="d-inline-block mr-2">
                        <fieldset>
                            <div class="vs-radio-con">
                                <input type="radio" id="data-primary" name="iconcolor" checked value="primary">
                                <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Azul</span>
                            </div>
                        </fieldset>
                    </li>
                    <li class="d-inline-block mr-2">
                        <fieldset>
                            <div class="vs-radio-con vs-radio-success">
                                <input type="radio" id="data-success" name="iconcolor" value="success">
                                <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Verde</span>
                            </div>
                        </fieldset>
                    </li>
                    <li class="d-inline-block mr-2">
                        <fieldset>
                            <div class="vs-radio-con vs-radio-danger">
                                <input type="radio" id="data-danger" name="iconcolor" value="danger">
                                <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Vermelho</span>
                            </div>
                        </fieldset>
                    </li>
                    <li class="d-inline-block mr-2">
                        <fieldset>
                            <div class="vs-radio-con vs-radio-info">
                                <input type="radio" id="data-info"name="iconcolor" value="info">
                                <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Cian</span>
                            </div>
                        </fieldset>
                    </li>
                    <li class="d-inline-block">
                        <fieldset>
                            <div class="vs-radio-con vs-radio-warning">
                                <input type="radio" id="data-warning" name="iconcolor" value="warning">
                                <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Laranja</span>
                            </div>
                        </fieldset>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 data-field-col">
                <label for="data-order">Ordem: </label>
                <div class="input-group">
                    <input type="number" id="data-order" name="order" class="touchspin" value="1">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="add-data-footer d-flex justify-content-around px-3 mt-2 text-center">
    <div class="add-data-btn">
        <button class="action-subimit btn btn-primary">Salvar</button>
    </div>
    <div class="cancel-data-btn">
        <button class="btn btn-outline-danger">Cancelar</button>
    </div>
</div>
<div id="type-hidden"></div>
