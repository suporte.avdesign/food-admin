<div class="modal-header bg-primary">
  <h4 class="modal-title" id="titleModalPlans">Editar Recurso</h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
</div>

<form id="form-plans" action="{{url("planos/$slug/details/$data->id")}}" class="form form-horizontal" onsubmit="return false">
  <div class="modal-body">
    @include('pages.plans.details._partials.form')
  </div>
  <div class="modal-footer">
    <div class="col-12">
      <button type="reset" id="btn-reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light d-none">Limpar</button>
      <button type="submit" id="btn-submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Salvar</button>
    </div>
  </div>
  <div id="methods" class="d-none">
    @csrf
    @method('put')
  </div>
</form>
