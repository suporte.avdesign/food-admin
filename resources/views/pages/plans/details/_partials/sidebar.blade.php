<div class="add-new-data-sidebar">
    <div class="overlay-bg"></div>
    <div class="add-new-data">
        <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
            <div>
                <h4 class="text-uppercase">Adicionar Recursos</h4>
            </div>
            <div class="hide-data-sidebar">
                <i class="feather icon-x"></i>
            </div>
        </div>

        {{-- incude form  sidebar starts --}}
        <form id="form-plan-resources" onsubmit="return false">
            @csrf
            @include('pages.plans.details._partials.form')
        </form>
    </div>
</div>