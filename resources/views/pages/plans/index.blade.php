@extends('layouts/contentLayoutMaster')

@section('title', 'Planos')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/ui/prism.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')) }}">


@endsection
@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/pages/plans.css')) }}">
@endsection
@section('content')
      <!-- Analytics card section start -->
      <section id="plans-card">
            <button class="btnAddPlan btn btn-primary mb-2 waves-effect waves-light">
                <i class="feather icon-plus"></i> Adicionar Plano
            </button>
            <div class="row">
                @foreach($plans as $plan)
                    <div id="{{$plan->slug}}" class="col-lg-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex justify-content-between align-items-end">
                                <h4 class="card-title">{{$plan->order}} - {{$plan->name}}</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <button data-slug="{{$plan->slug}}" class="btnRemovePlan btn btn-sm btn-icon btn-outline-danger mr-1 mb-1 waves-effect waves-light">
                                            <i class="text-danger feather icon-trash-2"></i>
                                        </button>
                                        <button data-slug="{{$plan->slug}}" class="btnEditPlan btn btn-sm btn-icon btn-outline-dark mr-1 mb-1 waves-effect waves-light">
                                            <i class="feather icon-edit"></i>
                                        </button>
                                        <button data-slug="{{$plan->slug}}" class="btnProfiles btn btn-sm btn-icon btn-outline-dark mr-1 mb-1 waves-effect waves-light">
                                            <i class="feather icon-user-plus"></i> <span class="text-bold-600">Perfis</span>
                                        </button>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body pt-50">
                                    <div id="session-chart" class="mb-1">
                                      {{$plan->description}}
                                    </div>
                                    @foreach($plan->profiles as $profile)
                                        <div class="series-info d-inline-block mr-2 mb-1">
                                            <span class="bullet bullet-success"></span>
                                            <span>{{$profile->name}}</span>
                                        </div>
                                    @endforeach
                                    <div class="chart-info d-flex justify-content-between mb-1">
                                        <div class="series-info d-flex align-items-center">
                                            @if($plan->active == 1)
                                                <span class="bullet bullet-success"></span>
                                                <span class="text-bold-600 mx-50">Ativo</span>
                                            @else
                                                <span class="bullet bullet-danger"></span>
                                                <span class="text-bold-600 mx-50">Inativo</span>
                                            @endif
                                        </div>
                                        <div class="series-result">
                                            <span class="text-bold-600 mx-50">R$ {{number_format($plan->price, 2, ',', '.')}}</span>
                                        </div>
                                    </div>
                                    <hr>
                                    @foreach($plan->details as $detail)
                                        <div id="resource-{{$detail->id}}" class="chart-info d-flex justify-content-between mb-1">
                                            <div class="series-info d-flex align-items-center">
                                                <i class="feather icon-{{$detail->icon}} font-medium-2 text-{{$detail->iconcolor}}"></i>
                                                <span>&nbsp; {!! $detail->resource !!}</span>
                                            </div>

                                            <div class="series-result">
                                                <ul class="list-inline mb-0">
                                                    <li><i class="feather @if ($detail->active == 1)text-success @else text-danger @endif icon-check-circle"></i></li>
                                                    <li><a class="action-delete" data-plan="{{$plan->slug}}" data-id="{{$detail->id}}"><i class="feather text-danger icon-trash-2"></i></a></li>
                                                    <li><a class="action-edit" data-plan="{{$plan->slug}}" data-id="{{$detail->id}}"><i class="feather icon-edit"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach
                                </div>
                                <div class="text-center">
                                    <button type="button" class="action-add btn mr-1 mb-1 btn-primary waves-effect waves-light" data-id="{{$plan->id}}" data-plan="{{$plan->slug}}">
                                        <i class="feather icon-plus"></i> Adicionar Recursos
                                    </button>
                                </div>
                            </div>
                      </div>
                    </div>
                @endforeach
            </div>
            {{-- Add and edit resources sidebar starts --}}
            @include('pages.plans.details._partials.sidebar')
            {{-- Modal --}}
            @include('pages.plans._partials.modal')
        </section>

      <section id="profiles-settings"></section>


@endsection
@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/editors/quill/highlight.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/editors/quill/quill.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/number-input.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/functions/useful.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/modal/components-modal.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/editors/custon-editor.js')) }}"></script>
  <script>
    let _configPlans = {!! json_encode([
        "url" => url('planos'),
        "url_create" => route('planos.create'),
        "close" => "Fechar",
        "error" => "Dados fornecidos inválidos.",
        "token" => csrf_token()
    ]) !!};
  </script>
  <script src="{{ asset(mix('js/scripts/pages/plans-resources.js')) }}"></script>
@endsection
