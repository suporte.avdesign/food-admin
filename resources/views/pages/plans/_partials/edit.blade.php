<div class="modal-header bg-primary">
  <h4 class="modal-title" id="titleModalPlans">Editar {{$data->name}}</h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
</div>

<form id="form-plans" action="{{route('planos.update', $data->slug)}}" class="form form-horizontal" onsubmit="return false">
  <div class="modal-body">
    @include('pages.plans._partials.form')
  </div>
  <div class="modal-footer">
    <div class="col-12">
      <button type="submit" id="btn-submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Alterar</button>
    </div>
  </div>
  <div class="d-none">
    @csrf
    @method('put')
  </div>
</form>
