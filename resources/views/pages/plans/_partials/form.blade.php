<div id="messages-modal"></div>

<label>Nome: </label>
<div class="form-group">
  <div class="controls">
    <input type="text" id="name" name="name" value="{{$data->name ?? ''}}" class="form-control"  placeholder="Nome do plano">
  </div>
</div>

<label>Preço: </label>
<div class="form-group">
  <div class="controls">
    <input type="text" id="price" name="price" value="{{$data->price ?? ''}}"placeholder="Valor do plano" class="form-control">
  </div>
</div>

<label>Descrição: </label>
<div class="form-group">
  <div class="controls">
      <textarea class="form-control" id="description" name="description" rows="3" placeholder="Descição do plano...">{{$data->description ?? ''}}</textarea>
  </div>
</div>
<div class="form-body">
  <div class="form-group row">
    <div class="col-md-8">
      <label>Status: </label>
      <ul class="list-unstyled mb-0">
        <li class="d-inline-block mr-2">
          <fieldset>
            <div class="vs-radio-con vs-radio-success">
              @isset($data)
                <input type="radio" name="active" value="1" @if($data->active == 1) checked @endif>
              @else
                <input type="radio" name="active" value="1" checked>
              @endisset
              <span class="vs-radio">
                  <span class="vs-radio--border"></span>
                  <span class="vs-radio--circle"></span>
              </span>
              <span class="">Ativo</span>
            </div>
          </fieldset>
        </li>
        <li class="d-inline-block mr-2">
          <fieldset>
            <div class="vs-radio-con vs-radio-danger">
              @isset($data)
                <input type="radio" name="active" value="0" @if($data->active == 0) checked @endif>
              @else
                <input type="radio" name="active" value="0">
              @endisset
              <span class="vs-radio">
                  <span class="vs-radio--border"></span>
                  <span class="vs-radio--circle"></span>
              </span>
              <span class="">Inativo</span>
            </div>
          </fieldset>
        </li>
      </ul>
    </div>
    <div class="col-md-4">
      <label>Ordem: </label>
      <div class="input-group">
        <input type="number" name="order" class="touchspin" value="{{$data->order ?? 1}}">
      </div>
    </div>
  </div>
</div>
<script src="{{ asset(mix('js/scripts/forms/number-input.js')) }}"></script>
<script>
  $('#btn-submit').on('click', function() {
    let form = $('#form-plans'),
      url = form.attr('action');
    $.ajax({
      type: 'POST',
      dataType: "json",
      url: url,
      data: form.serialize(),
      beforeSend: function() {
        $('#btn-submit').prop("disabled", true);
      },
      success: function(data){
        if (data.success == true) {
          let msg = displayAlert('success', data.message)
          $('#btn-reset').trigger('click');
          $('#messages-modal').html(msg);

          setTimeout(function() {
            $('#modalPlans').modal('hide');
            window.location.href = _configPlans.url;
          }, 3000);

        } else {
          alert('Algo aconteceu');
          return false;
        }
      },
      error: function(xhr) {
        $('#btn-submit').prop("disabled", false);
        let msg = errorStatus(xhr);
        $('#messages-modal').html(msg);
        setTimeout(function() {
          $('#messages-modal').html('');
        }, 3000);
      }

    });
  });

</script>
