<div class="card">
    <div class="card-content">
        <div class="card-body">
            <div class="tab-content">
                <div id="return-messages-profiles"></div>
                @foreach($profiles as $profile)
                    <div class="tab-pane @if($loop->index == 0) active @else fade @endif" id="profile-tab-{{$profile->slug}}" role="tabpanel" aria-labelledby="{{$profile->slug}}" aria-expanded="false">
                        <div class="row">
                            <h6 class="m-1">{{$profile->name}}: {{$profile->description}}</h6>
                            <div class="col-12 mb-1">
                                <div class="custom-control custom-switch custom-switch-success custom-control-inline">
                                    <input type="checkbox" onchange="sendPLanProfiles('{{$loop->index}}','{{$plan->slug}}')" class="custom-control-input" id="profile-{{$loop->index}}" value="{{$profile->id}}" checked/>
                                    <label class="custom-control-label mr-1" for="profile-{{$loop->index}}"></label>
                                    <span class="switch-label w-100"></span>
                                    <span class="label-profile-{{$plan->slug}}">Ativo</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
