<ul class="nav nav-pills flex-column mt-md-0 mt-1">
    <li class="nav-item">
        <button onclick="addPLanProfiles('{{url("planos/$plan->slug/profiles")}}')" class="btn btn-outline-primary mr-1 mb-1 waves-effect waves-light">
            <i class="feather icon-plus"></i> Adicionar Perfil
        </button>
    </li>
    @foreach($profiles as $profile)
        <li class="nav-item">
            <a class="nav-link d-flex py-75 @if($loop->index == 0) active @endif" id="profile-{{$profile->slug}}" data-toggle="pill"
               href="#profile-tab-{{$profile->slug}}" aria-expanded="true">
                <i class="{{$profile->icon}} mr-50 font-medium-3"></i>
                {{$profile->name}}
            </a>
        </li>
    @endforeach
</ul>
