<div class="content-header-left col-md-9 col-12 mb-2">
    <div class="row breadcrumbs-top">
        <div class="col-12">
            <h2 class="content-header-title float-left mb-0">{{$plan->name}}</h2>
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="dashboard-analytics"> Home </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="planos"> Planos </a>
                    </li>
                    <li class="breadcrumb-item">
                        {{$plan->name}}
                    </li>
                </ol>

            </div>
        </div>
    </div>
</div>
