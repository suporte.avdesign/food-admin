@if(count($profiles) > 0)
    <section id="basic-switches">
        <div class="row">
            <div class="col-sm-12">
                <p>Selecione os<code> Perfis </code> para vincular ao plano.</p>
                @foreach($profiles as $profile)
                    <div class="custom-control custom-switch custom-switch-success">
                        <input type="checkbox" class="custom-control-input" id="{{$profile->slug}}" name="profile[]" value="{{$profile->id}}">
                        <label class="custom-control-label" for="{{$profile->slug}}"></label>
                        <strong class="switch-label"><code>{{$profile->name}}:</code></strong><span> {{$profile->description}}</span>
                    </div>
                    <hr>
                @endforeach
            </div>
        </div>
    </section>
@else
    <p class="text-danger text-center">Não existe permissões para este modulo.</p>
@endif