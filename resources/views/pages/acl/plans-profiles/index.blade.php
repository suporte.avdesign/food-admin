@include('pages.acl.plans-profiles._partials.breadcrumb')
<div class="row">
    <!-- left sidbar section -->
    <div class="col-md-3 mb-2 mb-md-0">
        @include('pages.acl.plans-profiles._partials.sidebar')
    </div>
    <!-- right content section -->
    <div class="col-md-9">
        @include('pages.acl.plans-profiles._partials.form')
    </div>
</div>
@include('pages.acl.plans-profiles._partials.modal')