{{-- DataTable starts --}}
<div class="table-responsive">
    <table class="table data-list-view">
        <thead>
        <tr>
            <th width="20px">PERMISSÔES</th>
            <th>PERFIL</th>
            <th>DESCRIÇÃO</th>
            <th>AÇÕES</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($profiles as $profile)
            <tr id="{{ $profile->slug }}">
                <td class="font-medium-3 text-danger">
                    <span class="permission-action-edit" data-name="{{$profile->name}}" data-link="{{ route('profiles.permissions', $profile->slug) }}"><i class="feather icon-unlock"></i></span>
                </td>
                <td class="profile-name">{{ $profile->name }}</td>
                <td class="profile-description">{!! $profile->description !!}</td>
                <td class="profile-action font-medium-3">
                    <span data-slug="{{ $profile->slug }}" class="action-delete text-danger"><i class="feather icon-trash-2"></i></span>
                    <span data-slug="{{ $profile->slug }}" data-id="{{ $profile->id }}" class="action-edit"><i class="feather icon-edit"></i></span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
{{-- DataTable ends --}}