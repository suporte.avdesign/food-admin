<div class="add-new-data-sidebar">
    <div class="overlay-bg"></div>
    <div class="add-new-data">
        <form id="form-profile-permissions" method="POST" onsubmit="return false">
            @csrf
            <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
                <div>
                    <h4 class="text-uppercase">
                        <i class="feather icon-lock mr-50"></i><span class="title-header-sidbar"></span>
                    </h4>
                </div>
                <div class="hide-data-sidebar">
                    <i class="feather icon-x"></i>
                </div>
            </div>

            <div class="table-responsive">
                <h6 class="border-bottom py-1 mx-1 mb-0 font-medium-2">
                    <button type="button" id="btn-profile-permissions" data-link="" class="action-table-permissions btn btn-outline-primary mr-1 mb-1 waves-effect waves-light">
                        <i class="feather icon-user-check"></i> Permitidas
                    </button>
                    <button type="button" id="btn-add-permissions" data-link="" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">
                        <i class="feather icon-plus"></i> Permissões
                    </button>
                </h6>
                <div class="data-profile-permissions"></div>
                <div class="data-permissions-available"></div>
                <div id="return-messages" class="text-center"></div>
            </div>
            <div class="add-data-footer d-flex justify-content-around px-3 mt-2">
                <div id="btn-add-data-permissions">
                    <input  type="submit" class="permission-action-subimit btn btn-primary" value="Adicionar">
                </div>
                <div class="cancel-data-btn">
                    <input type="reset" class="btn btn-outline-danger" value="Fechar">
                </div>
            </div>
        </form>
    </div>
</div>