<div class="table-responsive">
    <h6 class="border-bottom py-1 mx-1 mb-0 font-medium-2">
        <i class="feather icon-lock mr-50"></i> Disponivel para {{$profile->name}}
    </h6>
    <table class="table table-borderless">
        <thead>
        <tr>
            <th>Modulos</th>
            <th>Selecione para permitir</th>
        </tr>
        </thead>
        <tbody>
        @foreach($permissions as $permission)
            <tr class="text-center">
                <td>{{$permission->name}}</td>
                <td>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="permissions[]" value="{{$permission->id}}">
                        <label class="custom-control-label" for="permissio-{{$permission->id}}">Teste</label>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>