<table class="table table-borderless">
    <thead>
    <tr class="text-center">
        <th class="text-black-50 font-medium-3">Modulos</th>
        <th class="text-warning font-medium-3"><i class="feather icon-eye"></i></th>
        <th class="text-primary font-medium-3"><i class="feather icon-plus-circle"></i></th>
        <th class="text-success font-medium-3"><i class="feather icon-edit"></i></th>
        <th class="text-danger font-medium-3"><i class="feather icon-trash-2"></i></th>
    </tr>
    </thead>
    <tbody>
        @foreach($permissions as $permission)
        <tr class="text-center">
            <td>{{$permission->name}}</td>
            <td>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" id="{{$permission->slug}}-view" name="permissions[]" value="{{$permission->id}}" class="custom-control-input" >
                    <label class="custom-control-label" for="{{$permission->slug}}-view"></label>
                </div>
            </td>
            <td>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" id="{{$permission->slug}}-add"  value="{{$permission->id}}-add" class="custom-control-input" checked disabled>
                    <label class="custom-control-label" for="{{$permission->slug}}-add"></label>
                </div>
            </td>
            <td>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" id="{{$permission->slug}}-edit" value="{{$permission->id}}|edit" class="custom-control-input" checked disabled>
                    <label class="custom-control-label" for="{{$permission->slug}}-edit"></label>
                </div>
            </td>
            <td>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" id="{{$permission->slug}}-delete"  value="{{$permission->id}}|delete" class="custom-control-input" checked disabled>
                    <label class="custom-control-label" for="{{$permission->slug}}-delete"></label>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>



