<div class="modal fade" id="modalPermissions" tabindex="-1" role="dialog"
     aria-labelledby="modalPermissionsTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <form id="form-permissions" onsubmit="return false">
                @csrf
                <div class="modal-header bg-primary white">
                    <h5 class="modal-title">Permissões</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer text-center">
                    <button type="button" class="action-close btn btn-secondary white">Fechar</button>
                    <button type="button" class="action-submit btn btn-primary">Salvar</button>
                </div>
                <div id="type-hidden" class="d-none"></div>
            </form>
        </div>
    </div>
</div>
