{{-- DataTable starts --}}
<div class="table-responsive">
    <table class="table data-list-view">
        <thead>
        <tr>
            <th>PERFIS</th>
            <th>NOME</th>
            <th>DESCRIÇÃO</th>
            <th>AÇÕES</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($permissions as $permission)
            <tr id="{{ $permission->slug }}">
                <td class="permission-action">
                    <span data-slug="{{ $permission->slug }}" class="action-profiles-permission text-success"><i class="feather icon-users"></i></span>
                </td>
                <td class="permission-name">{{ $permission->name }}</td>
                <td class="permission-description">{!! $permission->description !!}</td>
                <td class="permission-action">
                    <span data-slug="{{ $permission->slug }}" data-id="{{ $permission->id }}" class="action-edit"><i class="feather icon-edit"></i></span>
                    <span data-slug="{{ $permission->slug }}" class="action-delete"><i class="feather icon-trash"></i></span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
{{-- DataTable ends --}}