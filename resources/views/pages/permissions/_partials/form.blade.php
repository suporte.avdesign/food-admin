<div id="return-messages"></div>
<label>Nome: </label>
<div class="form-group">
    <div class="controls">
        <input type="text" id="name" name="name" value="{{$data->name ?? ''}}" class="form-control"  placeholder="Nome do plano">
    </div>
</div>
<label>Descrição: </label>
<div class="form-group">
    <div class="controls">
        <textarea class="form-control" id="description" name="description" rows="3" placeholder="Descição do plano...">{{$data->description ?? ''}}</textarea>
    </div>
</div>

