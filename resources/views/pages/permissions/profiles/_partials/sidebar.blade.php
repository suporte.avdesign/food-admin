<div class="add-new-data-sidebar">
    <div class="overlay-bg"></div>
    <div class="add-new-data">
        <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
            <div>
                <h4 class="text-uppercase">
                    <i class="feather icon-lock mr-50"></i><span class="title-header-sidbar"></span>
                </h4>
            </div>
            <div class="hide-data-sidebar">
                <i class="feather icon-x"></i>
            </div>
        </div>

        <div class="table-responsive">
            <div class="data-permission-profiles"></div>
            <div id="return-messages" class="text-center"></div>
        </div>

    </div>
</div>