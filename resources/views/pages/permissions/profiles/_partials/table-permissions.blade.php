@if(count($profiles) > 0)
    <table class="table table-borderless">
        <thead>
        <tr class="text-center">
            <th class="text-black-50 font-medium-3">Modulos</th>
            <th class="text-warning font-medium-3" data-original-title="Tooltip on top" aria-describedby="tooltip884865"><i class="feather icon-eye"></i></th>
            <th class="text-primary font-medium-3"><i class="feather icon-plus-circle"></i></th>
            <th class="text-success font-medium-3"><i class="feather icon-edit"></i></th>
            <th class="text-danger font-medium-3"><i class="feather icon-trash-2"></i></th>
        </tr>
        </thead>
        <tbody>
        @foreach($profiles as $profile)
            <tr id="{{$profile->id}}" class="text-center">
                <td>{{$profile->name}}</td>
                <td>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="{{$profile->slug}}-view" class="custom-control-input" checked disabled>
                        <label class="custom-control-label" for="{{$profile->slug}}-view"></label>
                    </div>
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="{{$profile->slug}}-add"  class="custom-control-input" checked disabled>
                        <label class="custom-control-label" for="{{$profile->slug}}-add"></label>
                    </div>
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="{{$profile->slug}}-edit" class="custom-control-input" checked disabled>
                        <label class="custom-control-label" for="{{$profile->slug}}-edit"></label>
                    </div>
                </td>
                <td>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="{{$profile->slug}}-delete" class="custom-control-input" checked disabled>
                        <label class="custom-control-label" for="{{$profile->slug}}-delete"></label>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <p class="text-danger text-center">Não existe permissões para este modulo.</p>
@endif
