@extends('layouts/contentLayoutMaster')

@section('title', 'Permissões')

@section('vendor-style')
    {{-- vendor files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
@endsection
@section('content')
    {{-- Data list view starts --}}
    <section id="data-list-view" class="data-list-view-header">
        {{-- dataTable starts --}}
        @include('pages.permissions._partials.table')
        {{-- Modal --}}
        @include('pages.permissions._partials.modal')
        {{-- Profiles Permission sidebar starts --}}
        @include('pages.permissions.profiles._partials.sidebar')
    </section>
    {{-- Data list view end --}}
@endsection
@section('vendor-script')
    {{-- vendor js files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.select.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/functions/useful.js')) }}"></script>
    <script>
        const _configPermissions = {!! json_encode([
        "url" => url('permissoes'),
        "url_permissions" => url('permissions'),
        'textPermissions' => 'Permissões',
        'textAdd' => 'Adicionar Permissão',
        'textUpdate' => 'Editar Permissão',
        "error" => "Erro",
        "textError" => "Dados fornecidos inválidos.",
        "close" => "Fechar",
        "dataTable" => array(
            "aLengthMenu" => [[30, 50, 70], [30, 50, 70]],
            "pageLength" => 30,
            "order" => [[0, "asc"]]
        ),
        "token" => csrf_token()
    ]) !!};
    </script>
    <script src="{{ asset(mix('js/scripts/pages/permissions-list-view.js')) }}"></script>
 artisan @endsection