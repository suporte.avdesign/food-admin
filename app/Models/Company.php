<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'plan_id',
        'uuid',
        'name',
        'email',
        'phone',
        'document',
        'slug',
        'logo',
        'active',
        'subscription',
        'expires_at',
        'subscription_id',
        'subscription_active',
        'subscription_suspended'
    ];
    
    /**
     * Retorna todos os usuários vinculados a Empresa.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    
    }
    
    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }
}
