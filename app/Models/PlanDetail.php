<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanDetail extends Model
{
    protected $fillable = ['plan_id', 'resource', 'icon', 'iconcolor', 'order', 'active'];

  /**
   * Retona o plano específico.
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }
}
