<?php

namespace App\Models;

use App\Models\ACL\Profile;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = ['name', 'slug', 'price', 'description', 'order', 'active'];
    
    
   /**
   * Retorna os detalhes de um plano específico.
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
    public function details()
    {
      return $this->hasMany(PlanDetail::class);
    }
    
    /**
     * Retorna todos as Empresas vinculadas ao plano.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companies()
    {
        return $this->hasMany(Company::class);
    }
    
    
    /**
     * Retorna os perfis de um plano.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function profiles()
    {
        return $this->belongsToMany(Profile::class);
    }
    
    
    public function profilesAvailable()
    {
        $profiles = Profile::whereNotIn('id', function ($query) {
            $query->select('plan_profile.profile_id');
            $query->from('plan_profile');
            $query->whereRaw("plan_id={$this->id}");
        
        })->paginate();
    
        return $profiles;
    }
    
}
