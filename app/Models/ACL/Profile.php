<?php

namespace App\Models\ACL;

use App\Models\Plan;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['name', 'slug', 'icon', 'description'];
    
    
    
    /**
     * Get Prmissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
    
    /**
     * Get permissions vinculadas ao perfil.
     * @return mixed
     */
    public function permissionsAvailable()
    {
        $permissions = Permission::whereNotIn('id', function ($query) {
            $query->select('permission_profile.permission_id');
            $query->from('permission_profile');
            $query->whereRaw("profile_id={$this->id}");
            
        })->paginate();
        
        return $permissions;
    }
    
    
    /**
     * Get Plans vinculados ao perfil.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function plans()
    {
        return $this->belongsToMany(Plan::class);
    }
    
    
}
