<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubjectContact extends Model
{
    protected $fillable = [
        'title',
        'department',
        'name',
        'phone',
        'email'
    ];
}
