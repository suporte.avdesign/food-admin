<?php

namespace App\Providers;

use App\Models\{ Plan, Company, PlanDetail };
use App\Models\ACL\{ Profile, Permission };
use App\Observers\{ PlanObserver, CompanyObserver, PlanDetailsObserver };
use App\Observers\ACL\{ ProfileObserver, PermissionsObserver };

use Laravel\Passport\Passport;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Passport::routes();

        Plan::observe(PlanObserver::class);
        Profile::observe(ProfileObserver::class);
        Company::observe(CompanyObserver::class);
        PlanDetail::observe(PlanDetailsObserver::class);
        Permission::observe(PermissionsObserver::class);
    }
}
