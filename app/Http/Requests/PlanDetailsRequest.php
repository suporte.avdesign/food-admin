<?php
    
    namespace App\Http\Requests;
    
    use Illuminate\Foundation\Http\FormRequest;
    
    class PlanDetailsRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
        
        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'resource' => 'required|min:12|max:191',
                'icon' => "required|min:3|max:30",
                'order' => 'required|min:1',
            ];
        }
        
        public function messages()
        {
            return [
                'resource.required' => 'O recurso é obrigatório',
                'resource.min' => 'O recurso deverá conter no mínimo 12 caracteres.',
                'resource.max' => 'O recurso não deverá conter mais de 191 caracteres.',
                'icon.required' => 'O icone é obrigatório.',
                'icon.min' => 'O icone deverá conter no mínimo 3 caracteres.',
                'icon.max' => 'O icone não deverá conter mais de 30 caracteres.',
                'order.required' => 'A ordem é obrigatória.',
                'order.min' => 'A ordem deverá conter no mínimo 1 número.',
            ];
        }
    }
