<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class PlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $slug = Str::slug($this->get('name'));

        return [
            'name' => "required|min:3|max:255|unique:plans,name,{$slug},slug",
            'description' => 'required|min:10|max:50',
            'price' => "required|regex:/^\d+(\.\d{1,2})?$/",
            'order' => 'required|min:1',
        ];
    }

    public function messages()
    {
      return [
          'name.required' => 'O nome é obrigatório',
          'name.min' => 'O nome deverá conter no mínimo 3 caracteres.',
          'name.max' => 'O nome não deverá conter mais de 50 caracteres.',
          'name.unique' => 'Este nome já se encontra utilizado.',
          'description.required' => 'A descrição é obrigatória.',
          'description.min' => 'A descrição deverá conter no mínimo 3 caracteres.',
          'description.max' => 'A descrição não deverá conter mais de 255 caracteres.',
          'price.required' => 'O preço é obrigatório',
          'price.regex' => 'O formato do valor do preço é inválido.',
          'order.required' => 'A ordem é obrigatória.',
          'order.min' => 'A ordem deverá conter no mínimo 1 número.',
      ];
    }
}
