<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->get('id');
        return [
            'name' => "required|min:3|max:255|unique:profiles,name,{$id},id",
            'description' => 'required|min:10',
        ];
    }
    
    
    public function messages()
    {
        return [
            'name.required' => 'O nome é obrigatório',
            'name.min' => 'O nome deverá conter no mínimo 3 caracteres.',
            'name.max' => 'O nome não deverá conter mais de 50 caracteres.',
            'name.unique' => 'Este nome já se encontra utilizado.',
            'description.required' => 'A descrição é obrigatória.',
            'description.min' => 'A descrição deverá conter no mínimo 10 caracteres.'
        ];
    }
}
