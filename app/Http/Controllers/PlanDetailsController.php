<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use App\Models\PlanDetail;
use App\Http\Requests\PlanDetailsRequest;


class PlanDetailsController extends Controller
{
    private $plan;
    private $model;
    private $view = 'pages.plans.details._partials';
    
    public function __construct(Plan $plan, PlanDetail $model)
    {
        $this->middleware('auth');
    
        $this->plan = $plan;
        $this->model = $model;
    }
    
    /**
     * Retorna o modal e form do plano específico.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $plan = $this->plan->whereSlug($slug)->first();
        $form = view("{$this->view}.create", compact('plan'))->render();
        $out = [
            'success' => true,
            'modal' => $form
        ];
        
        return response()->json($out);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlanDetailsRequest $request, $slug)
    {
        $data = $this->model->create($request->except('_token'));
        if ($data) {
            return response()->json(['success' => true, 'message' => 'O Recurso foi salvo.']);
        } else {
            return response()->json(['message' => 'Houve um erro não identificado, tente mais tarde.']);
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug, $id)
    {
        $data = $this->model->find($id);
        if (!$data) {
            $out = ['message' => 'Houve um erro não identificado, tente mais tarde.'];
        } else {
            $out = [
                'success' => true,
                'resource' => $data->resource,
                'icon' => $data->icon,
                'active' => $data->active,
                'order' => $data->order,
                'iconcolor' => $data->iconcolor
            ];
        }
        
        return response()->json($out);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PlanDetailsRequest $request, $slug, $id)
    {
        $input = $request->except('_token', '_method');
        
        $data = $this->model->find($id);
        if ( $data->update($input) )  {
            return response()->json(['success' => true, 'message' => 'O Recurso foi alterado com sucesso.']);
        } else {
            return response()->json(['message' => 'Houve um erro não identificado, tente mais tarde.']);
        }
    }
    
    /**
     * Remove the specified resource from storage.
     * @param  string  $slug
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug, $id)
    {
        $data = $this->model->find($id);
        $delete = $data->delete();
        if ($delete)
            $out = [
                'success' => true,
            ];
        
        return response()->json($out);
    }

}
