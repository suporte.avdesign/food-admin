<?php
    
    namespace App\Http\Controllers;
    
    use App\Models\Plan;
    use App\Services\Settings;
    use App\Http\Requests\PlanRequest;
    
    class PlanController extends Controller
    {
        private $model;
        private $config;
        private $view = 'pages.plans';
    
        public function __construct(Plan $model, Settings $config)
        {
            $this->middleware('auth');
            
            $this->model = $model;
            $this->config = $config;
            $this->pageConfigs = $this->config->pageConfigs('plan');
            $this->breadcrumbs = $this->config->breadcrumbs('plan');
        }
        
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $plans = $this->model->orderBy('order')->get();
            
            return view("{$this->view}.index", [
                'plans' => $plans,
                'pageConfigs' => $this->pageConfigs,
                'breadcrumbs' => $this->breadcrumbs
            ]);
        }
        
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $form = view("{$this->view}._partials.create")->render();
            $out = [
                'success' => true,
                'modal' => $form
            ];
            return response()->json($out);
        }
        
        /**
         * Store a newly created resource in storage.
         *
         * @param  \App\Http\Requests\PlanRequest $request
         * @return \Illuminate\Http\Response
         */
        public function store(PlanRequest $request)
        {
            $data = $this->model->create($request->except('_token'));
            if ($data) {
                return response()->json(['success' => true, 'message' => 'O Plano foi salvo.']);
            } else {
                return response()->json(['message' => 'Houve um erro não identificado, tente mais tarde.']);
            }
        }
        
        /**
         * Display the specified resource.
         *
         * @param  string  $slug
         * @return \Illuminate\Http\Response
         */
        public function show($slug)
        {
            $data = $this->model->whereSlug($slug)->first();
            $form = view("{$this->view}._partials.edit", ['data' => $data])->render();
            $out = [
                'success' => true,
                'modal' => $form
            ];
            
            return response()->json($out);
        }
        
        
        
        /**
         * Update the specified resource in storage.
         *
         * @param  \App\Http\Requests\PlanRequest $request
         * @param  string  $slug
         * @return \Illuminate\Http\Response
         */
        public function update(PlanRequest $request, $slug)
        {
            $input = $request->except('_token', '_method');
            
            $data = $this->model->whereSlug($slug)->first();
            if ( $data->update($input) )  {
                return response()->json(['success' => true, 'message' => 'O Plano foi alterado com sucesso.']);
            } else {
            
            }
            
        }
        
        /**
         * Remove the specified resource from storage.
         *
         * @param  string  $slug
         * @return \Illuminate\Http\Response
         */
        public function destroy($slug)
        {
            $data = $this->model->whereSlug($slug)->first();
            if ($data->delete())
                $out = [
                    'success' => true,
                ];
            
            return response()->json($out);
        }
    }
