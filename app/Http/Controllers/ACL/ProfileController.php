<?php

namespace App\Http\Controllers\ACL;

use App\Models\ACL\Profile;
use App\Services\Settings;
use App\Http\Controllers\Controller;

use App\Http\Requests\PermissionRequest;



class ProfileController extends Controller
{
    private $view;
    private $model;
    private $config;
    
    
    public function __construct(Profile $model, Settings $config)
    {
        $this->middleware('auth');
        
        $this->model = $model;
        $this->config = $config;
        $this->view = 'pages.acl.profiles';
        $this->pageConfigs = $this->config->pageConfigs('profile');
        $this->breadcrumbs = $this->config->breadcrumbs('profile');
    }
    
    /**
     * Exibir uma lista dos perfis.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = $this->model->all();
        
        return view("$this->view.index", [
            'breadcrumbs' => $this->breadcrumbs,
            'profiles' => $profiles
        ]);
    }
    
    /**
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("$this->view._partials.form");
    }
    
    /**
     * Criar Perfil
     *
     * @param  \App\Http\Requests\PermissionRequest;
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        $input = $request->except('_token');
        if ($this->model->create($input)) {
            return response()->json(['success' => true,'message' => 'Dados registrados com sucesso.']);
        } else {
            return response()->json(['message' => 'Houve um erro no servidor, tente mais tarde.']);
        }
    }
    
    /**
     * Obter o perfil especifico.
     *
     * @param string $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($slug)
    {
        $data = $this->model->whereSlug($slug)->first();
        if (!$data) {
            abort(404);
        }
        return view("$this->view._partials.form", compact('data'));
        
    }
    
    
    /**
     * Altera o perfil específico.
     *
     * @param  string  $slug
     * @param  \App\Http\Requests\PermissionRequest;
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionRequest $request, $slug)
    {
        $input = $request->except('_token', '_method');
        $data = $this->model->whereSlug($slug)->first();
        if ( !$data->update($input) )  {
        
        }
        return response()->json(['success' => true, 'message' => 'Dados alterados com sucesso.']);
        
    }
    
    /**
     * Remove o perfil especifico.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $data = $this->model->whereSlug($slug)->first();
        if ($data->delete())
            $out = [
                'success' => true
            ];
        
        return response()->json($out);
    }
}
