<?php

namespace App\Http\Controllers\ACL;

use App\Models\ACL\Permission;
use App\Services\Settings;
use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;

class PermissionController extends Controller
{
    private $view;
    private $model;
    private $config;
    
    public function __construct(Permission $model, Settings $config)
    {
        //$this->middleware('auth');
        
        $this->model = $model;
        $this->config = $config;
        $this->view = 'pages.permissions';
        $this->pageConfigs = $this->config->pageConfigs('permissions');
        $this->breadcrumbs = $this->config->breadcrumbs('permissions');
    }
    
    /**
     * Exibir uma lista das permissões.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = $this->model->orderBy('name')->get();
        
        return view("$this->view.index", [
            'breadcrumbs' => $this->breadcrumbs,
            'permissions' => $permissions
        ]);
    }
    
    /**
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("$this->view._partials.form");
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PermissionRequest;
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        $input = $request->except('_token');
        if ($this->model->create($input)) {
            return response()->json(['success' => true,'message' => 'Dados registrados com sucesso.']);
        } else {
            return response()->json(['message' => 'Houve um erro no servidor, tente mais tarde.']);
        }
    }
    
    /**
     * Display the specified resource.
     * @param string $slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($slug)
    {
        $data = $this->model->whereSlug($slug)->first();
        if (!$data) {
            abort(404);
        }
        return view("$this->view._partials.form", compact('data'));
    
    }
    
    
    /**
     * Update the specified resource in storage.
     *
     * @param  string  $slug
     * @param  \App\Http\Requests\PermissionRequest;
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionRequest $request, $slug)
    {
        $input = $request->except('_token', '_method');
        $data = $this->model->whereSlug($slug)->first();
        if ( !$data->update($input) )  {
        
        }
        return response()->json(['success' => true, 'message' => 'Dados alterados com sucesso.']);
    
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $data = $this->model->whereSlug($slug)->first();
        if ($data->delete())
            $out = [
                'success' => true
            ];
        
        return response()->json($out);
    }
}
