<?php

namespace App\Http\Controllers\ACL;

use App\Models\ACL\Permission;
use App\Models\ACL\Profile;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ProfilePermissionController extends Controller
{
    private $view;
    private $profile;
    private $permission;
    
    public function __construct(Profile $profile, Permission $permission)
    {
        $this->middleware('auth');
        $this->view = 'pages.acl.profiles.permissions';
        $this->profile = $profile;
        $this->permission = $permission;
    }
    
    
    /**
     * Lista as permissões de um perfil específico
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function permissions($slug)
    {
        $profile = $this->profile->whereSlug($slug)->first();
        if (!$profile) {
            abort(404, 'Não existe este perfil');
        }
        $permissions = $profile->permissions()->paginate();
        return view("$this->view._partials.table-profile", [
            'profile' => $profile,
            'permissions' => $permissions
        ]);
        
    }
    
    /**
     * Lista as permissões de um perfil específico
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function profiles($slug)
    {
        $permission = $this->permission->whereSlug($slug)->first();
        if (!$permission) {
            abort(404, 'Não existe este perfil');
        }
        $profiles = $permission->profiles()->paginate();
    
        return view("pages.permissions.profiles._partials.table-permissions", [
            'profiles' => $profiles
        ]);
        
    }

    /**
     * Obter permissões disponíveis para o perfil específico.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function permissionsAvailable($slug)
    {
        
        $profile = $this->profile->whereSlug($slug)->first();
        if (!$profile) {
            abort(404, 'Não existe este perfil');
        }
        $permissions = $profile->permissionsAvailable();
        return view("$this->view._partials.table-available", [
            'profile' => $profile,
            'permissions' => $permissions
        ]);
    }

    /**
     * Adicionar permissões a um perfil específico.
     *
     * @param string $slug
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function attachPermissionsProfile(Request $request, $slug)
    {
        $profile = $this->profile->whereSlug($slug)->first();
        if (!$profile) {
            abort(404, 'Não existe este perfil');
        }
        $selected = $request->permissions;
        if (!$selected || count($selected) == 0) {
            return response()->json(['message' => 'Por favor, selecione no mínimo 1 permissão']);
        }
        
        $profile->permissions()->attach($selected);
            
        return response()->json(['success' => true, 'message' => 'As permissões foram adicionada com suceso!']);
        
    }
    
    /**
     * Disvincular a permissão do perfil
     *
     * @param $slug
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detachPermissionsProfile($slug, $id)
    {
        $profile = $this->profile->whereSlug($slug)->first();
        $permission = $this->permission->find($id);
        if (!$profile || !$permission) {
            abort(404, 'Não existe o perfil ou a permissão.');
        }
        
        $profile->permissions()->detach($permission);
        
        return response()->json(['success' => true]);
    }
    
    
}
