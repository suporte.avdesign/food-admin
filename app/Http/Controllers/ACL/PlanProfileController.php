<?php

namespace App\Http\Controllers\ACL;

use App\Models\Plan;
use App\Models\ACL\Profile;
use App\Services\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class PlanProfileController extends Controller
{
    private $view;
    private $plan;
    private $config;
    private $profile;
    
    public function __construct(Plan $plan, Profile $profile, Settings $config)
    {
        $this->middleware('auth');
        
        $this->plan = $plan;
        $this->config = $config;
        $this->profile = $profile;
        $this->view = 'pages.acl.plans-profiles';
        $this->breadcrumbs = $this->config->breadcrumbs('plans-profiles');
    }
    
    
    
    /**
     * Display a listing of the resource.
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $plan = $this->plan->whereSlug($slug)->first();
        if (!$plan) {
            abort(404, 'Não existe este plano');
        }
        
        $profiles = $plan->profiles;
        $this->breadcrumbs[1]['name'] = $this->breadcrumbs[1]['name']." {$plan->name}";
    
        return view("$this->view.index", [
            'plan' => $plan,
            'profiles' => $profiles,
            'breadcrumbs' => $this->breadcrumbs
        ]);
    }
    
    /**
     * Adicionar perfil a um plano específico.
     *
     * @param  string $slug
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $slug)
    {
        $plan = $this->plan->whereSlug($slug)->first();
        if (!$plan) {
            abort(404, 'Não existe este perfil');
        }
        $selected = $request->profile;
        if (!$selected || count($selected) == 0) {
            return response()->json(['message' => 'Por favor, selecione no mínimo 1 perfil']);
        }
    
        $plan->profiles()->attach($selected);
    
        return response()->json(['success' => true, 'message' => 'Perfil adicionado com sucesso!']);
    }
    
    
    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $plan = $this->plan->whereSlug($slug)->first();
        if (!$plan) {
            abort(404, 'Não existe este perfil');
        }
        $profiles = $plan->profilesAvailable();
        
        return view("$this->view._partials.table-available", [
            'plan' => $plan,
            'profiles' => $profiles
        ]);
        
    }
    
    
    /**
     * Disvincular o perfil do plano
     *
     * @param  string $slug
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug, $id)
    {
        $plan = $this->plan->whereSlug($slug)->first();
        $profile = $this->profile->find($id);
        if (!$plan || !$profile) {
            abort(404, 'Não existe o olano ou o perfil.');
        }
    
        $plan->profiles()->detach($profile);
        return response()->json(['success' => true, 'message' => 'Perfil removido com sucesso!']);
    }
}
