<?php
    
namespace App\Services\Traits;


trait PagesConfig
{
    /**
     * Retorna breadcrumbs da página específica.
     *
     * @param $page
     * @return array
     */
    public static function breadcrumbs($page)
    {
        $config = [
            'permissions' => [
                self::home(), ['name'=>"Permissões"]
            ],
            'plan' => [
                self::home(), ['name'=>"Planos"]
            ],
            'plans-profiles' => [
                self::home(), ['name'=>"Perfis:"]
            ],
            'profile' => [
                self::home(), ['name'=>"Perfil"]
            ],
            'users' => [
                self::home(), ['name'=>"Usuários"]
            ],
        ];
        
        return $config[$page];
        
    }
    
    public static function pageConfigs($page)
    {
        $config = [
            'permissions' => [
                'navbarType' => 'sticky',
                'footerType' => 'sticky',
            ],
            'plan' => [
                'navbarType' => 'sticky',
                'footerType' => 'sticky',
            ],
            'profile' => [
                'navbarType' => 'sticky',
                'footerType' => 'sticky',
            ],
            'users' => [
                'navbarType' => 'sticky',
                'footerType' => 'sticky',
            ],

        ];
    
        return $config[$page];
    }
    
    
    
    /**
     * @return string[]
     */
    public static function home()
    {
        return [
            'link'=> "dashboard-analytics",
            'name'=> "Home"
        ];
    }
}