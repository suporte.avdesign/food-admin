<?php
/**
 * Açoes para criar Observers:
 * 1 - php artisan make:observer PlanObserver --model=Models\\Plan
 * 2 - Colocar os códigos nos métodos.
 * 3 - Registar PlanObserver em AppServiceProvider(boot)
 */

namespace App\Observers;

use App\Models\Plan;
use Illuminate\Support\Str;

class PlanObserver
{
    /**
     * Handle the plan "creating" event.
     *
     * @param  \App\Models\Plan  $plan
     * @return void
     */
    public function creating(Plan $plan)
    {
      $plan->order = $this->formatOrder($plan->order);
      $plan->slug = Str::slug($plan->name);
    }

    /**
     * Handle the plan "updating" event.
     *
     * @param  \App\Models\Plan  $plan
     * @return void
     */
    public function updating(Plan $plan)
    {
      $plan->order = $this->formatOrder($plan->order);
      $plan->slug = Str::slug($plan->name);
    }

  /**
   * Ordem com 2 caracteries no mínimo.
   *
   * @param $str
   * @return string
   */
    private function formatOrder($str)
    {
      $count = strlen($str);
      if ($count == 1) {
        $str = '0'.$str;
      }
      return $str;
    }


}
