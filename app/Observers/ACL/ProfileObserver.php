<?php

namespace App\Observers\ACL;

use App\Models\ACL\Profile;
use Illuminate\Support\Str;

class ProfileObserver
{
    /**
     * Handle the profile "creating" event.
     *
     * @param  \App\Models\Profile  $profile
     * @return void
     */
    public function creating(Profile $profile)
    {
        $profile->slug = Str::slug($profile->name);
    }

    /**
     * Handle the profile "updating" event.
     *
     * @param  \App\Models\Profile  $profile
     * @return void
     */
    public function updating(Profile $profile)
    {
        $profile->slug = Str::slug($profile->name);
    }

   
}
