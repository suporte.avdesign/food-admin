<?php

namespace App\Observers\ACL;

use App\Models\ACL\Permission;
use Illuminate\Support\Str;

class PermissionsObserver
{
    /**
     * Handle the permission "creating" event.
     *
     * @param  \App\Permission  $permission
     * @return void
     */
    public function creating(Permission $permission)
    {
        $permission->slug = Str::slug($permission->name);
    }

    /**
     * Handle the permission "updating" event.
     *
     * @param  \App\Permission  $permission
     * @return void
     */
    public function updating(Permission $permission)
    {
        $permission->slug = Str::slug($permission->name);
    }

}
