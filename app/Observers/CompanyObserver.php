<?php
    
    namespace App\Observers;
    
    use App\Models\Company;
    use Illuminate\Support\Str;
    
    class CompanyObserver
    {
        /**
         * Handle the company "creating" event.
         *
         * @param  \App\Models\Company  $company
         * @return void
         */
        public function creating(Company $company)
        {
            $this->uuid = Str::uuid();
            $this->slug = Str::kebab($company->name);
        }
        
        /**
         * Handle the company "updating" event.
         *
         * @param  \App\Models\Company  $company
         * @return void
         */
        public function updating(Company $company)
        {
            $this->uuid = Str::uuid();
            $this->slug = Str::kebab($company->name);
        }
    }
