<?php
/**
 * Açoes para criar Observers:
 * 1 - php artisan make:observer PlanDetailsObserver --model=Models\\PlanDetail
 * 2 - Colocar os códigos nos métodos.
 * 3 - Registar PlanDetailsObserver em AppServiceProvider(boot)
 */

namespace App\Observers;

use App\Models\PlanDetail;

class PlanDetailsObserver
{
    /**
     * Handle the plan detail "creating" event.
     *
     * @param  \App\Models\PlanDetail  $planDetail
     * @return void
     */
    public function creating(PlanDetail $planDetail)
    {
        $this->formatOrderResource($planDetail);
    }

    /**
     * Handle the plan detail "updating" event.
     *
     * @param  \App\Models\PlanDetail  $planDetail
     * @return void
     */
    public function updating(PlanDetail $planDetail)
    {
        $this->formatOrderResource($planDetail);
    }
    
    public function formatOrderResource($planDetail)
    {
        $count = strlen($planDetail->order);
        if ($count == 1) {
            $planDetail->order = '0'.$planDetail->order;
        }
    
        $p1 = str_replace('<p>', '', $planDetail->resource);
        $planDetail->resource = str_replace('</p>', '', $p1);
        
        return $planDetail;
    }

}
